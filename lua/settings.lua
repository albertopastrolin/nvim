local indent = 2

vim.opt.expandtab = true                  -- Use spaces instead of tabs
vim.opt.shiftwidth = indent               -- Size of an indent
vim.opt.smartindent = true                -- Insert indents automatically
vim.opt.softtabstop = indent              -- Number of spaces soft tabs count for
vim.opt.tabstop = indent                  -- Number of spaces tabs count for
vim.opt.undofile = true                   -- Enable undofiles
vim.opt.background = 'dark'               -- Sets the background to dark
vim.opt.completeopt = 'menuone,noselect'  -- Autocomplete menu
vim.opt.hidden = true                     -- Enable modified buffers in background
vim.opt.ignorecase = true                 -- Ignore case
vim.opt.joinspaces = false                -- No double spaces with join after a dot
vim.opt.mouse = 'a'                       -- Enable mouse
vim.opt.scrolloff = 4                     -- Lines of context
vim.opt.shiftround = true                 -- Round indent
vim.opt.sidescrolloff = 8                 -- Columns of context
vim.opt.smartcase = true                  -- Don't ignore case with capitals
vim.opt.splitbelow = true                 -- Put new windows below current
vim.opt.splitright = true                 -- Put new windows right of current
vim.opt.termguicolors = true              -- True color support
vim.opt.wildmode = 'longest:full,full'    -- Command-line completion mode
vim.opt.number = true                     -- Print line number
vim.opt.relativenumber = true             -- Relative line numbers
vim.opt.wrap = false                      -- Disable line 

-- Set colorscheme
vim.api.nvim_command "colorscheme tokyonight-moon"

-- System clipboard configuration, use unnamedplus in linux and unnamed in osx
if vim.fn.has('unix') then
  vim.opt.clipboard = 'unnamedplus'
elseif vim.fn.has('macunix') then
  vim.opt.clipboard = 'unnamed'
end

vim.cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'  -- disabled in visual mode

-- LSP warning messages configuration
vim.diagnostic.config({
  virtual_text = true,
  signs = true,
  underline = true,
  update_in_insert = false,
  severity_sort = false,
})

local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end
