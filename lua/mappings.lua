-- Base neovim mappings
vim.keymap.set('i', '<C-u>', '<C-g>u<C-u>') -- Make <C-u> undoable
vim.keymap.set('i', '<C-w>', '<C-g>u<C-w>') -- Make <C-w> undoable
vim.keymap.set('n', '<C-l>', '<cmd>noh<CR>') -- Clear highlights
vim.keymap.set('n', '<CR>', '<cmd>noh<CR><CR>') -- This unsets the last search pattern register by hitting return
vim.keymap.set('n', '<leader>o', 'm`o<Esc>``') -- Insert a newline in normal mode

-- LSP mappings
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, { noremap = true, silent = true })
vim.keymap.set('n', '<leader>f', vim.lsp.buf.format) -- Refactor the current buffer using LSP diagnostics
vim.keymap.set('n', '<space>m', vim.lsp.buf.rename) -- Rename a symbol
vim.keymap.set('n', '<space>r', vim.lsp.buf.references) -- Search all references of the current symbol
vim.keymap.set('n', '<space>s', vim.lsp.buf.document_symbol) -- Show all the symbols found in the current buffer
