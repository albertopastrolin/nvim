return {
  ---------------------- Must be loaded ASAP ---------------------
  {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 1000,
    config = true,
  },                                                   -- A clean, dark theme

  ---------------------- Utilities -------------------------------
  {
    'sindrets/diffview.nvim',
    dependencies = 'nvim-lua/plenary.nvim',
    event = 'VeryLazy'
  },                                                                  -- improved view to help with merges
  { "ellisonleao/glow.nvim", event = 'VeryLazy' },                    -- glow utility to render markdown directly in vim
  { 'chaoren/vim-wordmotion', event = 'VeryLazy' },                   -- Better word movements for CamelCase and snake_case
  { 'folke/which-key.nvim', event = 'VeryLazy', config = true },      -- Displays a popup with possible key bindings of the command you started typing
  { 'itchyny/vim-cursorword', event = 'VeryLazy' },                   -- Highlight the current word in the buffer
  { 'jghauser/mkdir.nvim', event = 'VeryLazy' },                      -- Create missing folders automatically on save
  { 'mbbill/undotree', event = 'VeryLazy' },                          -- Undotree
  { 'pechorin/any-jump.vim', event = 'VeryLazy' },                    -- Jump to definitions/references
  { 'wakatime/vim-wakatime', event = 'VeryLazy' },                    -- keep track of your worktime with wakatime
  { 'windwp/nvim-autopairs', event = 'InsertEnter', config = true },  -- Auto close parentheses using treesitter syntaxes
  { 'windwp/nvim-ts-autotag', event = 'InsertEnter', config = true }, -- Auto close HTML tags using treesitter syntaxes

  ---------------------- Fuzzy finder ----------------------------
  { 'ojroques/nvim-lspfuzzy', event = 'VeryLazy' }, -- Integrate the lsp client results with fzf

  --------------------------- Appearance -------------------------
  { 'kyazdani42/nvim-web-devicons', event = 'BufReadPost' }, -- Neovim icon font support
  { 'yamatsum/nvim-web-nonicons', event = 'BufReadPost' },   -- Neovim icon font support
  { 'karb94/neoscroll.nvim', event = 'VeryLazy' },           -- Improve scrolling
  { 'stevearc/dressing.nvim', event = 'VeryLazy' },          -- Neovim plugin to improve the default vim.ui interfaces

  ------------------------------- LSP ----------------------------
  'folke/lsp-trouble.nvim',                                           -- List of LSP issues
  { 'nvim-treesitter/nvim-treesitter-refactor', event = 'VeryLazy' }, -- Code refactoring using treesitter
  { 'p00f/nvim-ts-rainbow', event = 'BufReadPost' },                  -- color nested parentheses with different colors to help differentiate them
}
