return {
  'nvim-treesitter/nvim-treesitter',
  build = ":TSUpdate",
  event = "BufReadPost",
  dependencies = {
    "nvim-treesitter/nvim-treesitter-textobjects",
  },
  config = function ()
    require('nvim-treesitter.configs').setup {
      ensure_installed = 'all',
      rainbow = {
        enable = true,
        extended_mode = true,
      },
      highlight = { enable = true },
      refactor = {
        highlight_definitions = { enable = true },
        smart_rename = {
          enable = true,
          keymaps = {
            smart_rename = 'grr'
          }
        }
      }
    }
  end
}
