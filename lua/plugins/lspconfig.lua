local servers = require 'config.lsp_servers'

return {
  'neovim/nvim-lspconfig',
  event = "BufReadPre",
  dependencies = {
    'hrsh7th/cmp-nvim-lsp', -- lsp as a completion source
  },
  config = function ()
    local lspconfig = require 'lspconfig'
    local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

    for _, server in ipairs(servers) do
      lspconfig[server].setup { capabilities = capabilities }
    end

    -- load denols in deno projects and tsserver in node projects
    lspconfig.denols.setup {
      root_dir = lspconfig.util.root_pattern("deno.json", "deno.jsonc"),
    }

    lspconfig.tsserver.setup {
      root_dir = lspconfig.util.root_pattern("package.json"),
    }

    lspconfig.volar.setup {
      -- workaround to fix a loading issue
      filetypes = { "vue", "json" },
      -- use volar both in node and deno projects
      root_dir = lspconfig.util.root_pattern("package.json", "deno.json"),
    }

    -- set vim as a global value
    lspconfig.sumneko_lua.setup {
      settings = {
        Lua = {
          diagnostics = {
            globals = { 'vim' }
          }
        }
      }
    }
  end
}
