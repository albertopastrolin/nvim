local servers = require 'config.lsp_servers'

return {
  'williamboman/mason-lspconfig.nvim',
  {
    'williamboman/mason.nvim',
    event = "BufReadPost",
    config = function()
      require("mason").setup()
      require("mason-lspconfig").setup {
        ensure_installed = servers,
        automatic_installation = true
      }
    end
  }
}
