return {
  'L3MON4D3/LuaSnip',
  event = 'InsertEnter',
  dependencies = {
    "rafamadriz/friendly-snippets",
    config = function()
      require("luasnip.loaders.from_vscode").lazy_load()
    end,
  },
  config = function ()
    require('luasnip').setup({
      history = true,
      enable_autosnippets = true,
    })
  end
}
