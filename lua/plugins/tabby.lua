vim.keymap.set('i', '<C-t>', ':$tabnew<CR>')               -- New tab mapping
vim.keymap.set('n', '<C-Down>', 'gt')                      -- Previous tab
vim.keymap.set('n', '<C-Up>', 'gT')                        -- Next tab
vim.keymap.set('n', '<C-t>', '<cmd>$tabnew<CR>')           -- New tab mapping

return {
  'nanozuki/tabby.nvim',
  event = 'VeryLazy',
  config = true
}

