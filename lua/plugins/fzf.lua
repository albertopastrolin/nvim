return {
  'ibhagwan/fzf-lua',
  dependencies = 'vijaymarupudi/nvim-fzf',
  event = 'VeryLazy',
  config = function ()
    local fzf = require'fzf-lua'
    fzf.setup {
      win_height = 0.4,
      win_row = 1,
      fzf_layout = 'default',
      files = {
        cmd = 'fd -t f'
      }
    }

    vim.keymap.set('n', '<C-p>', fzf.files)              -- Files present in the current directory
    vim.api.nvim_create_user_command('Rg', fzf.grep, {}) -- Shortcut to search words in the current directory
  end
}
