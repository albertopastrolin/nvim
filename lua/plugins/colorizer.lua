return  {
  'NvChad/nvim-colorizer.lua',
  event = "BufReadPost",
  config = function ()
    require('colorizer').setup {
      'css';
      'javascript';
      html = {
        mode = 'foreground';
      }
    }
  end
} -- Highlight the colors in css files
