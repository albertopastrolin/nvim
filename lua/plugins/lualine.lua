return {
  'nvim-lualine/lualine.nvim',    -- Lightweight and simple statusline
  event = 'VeryLazy',
  config = function ()
    require('lualine').setup({
      options = {
        theme = 'tokyonight';
        section_separators = {left = '', right = ''},
        component_separators = {left = '', right = ''},
        icons_enabled = true,
      },
      sections = {
        lualine_a = { {'mode', upper = true} },
        lualine_b = { {'branch', icon = ''} },
        lualine_c = { {'filename', file_status = true} }
      },
      extensions = { 'fzf' }
    })
  end
}
