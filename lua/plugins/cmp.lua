local lsp_symbols = require 'config.lsp_symbols'

return {
  'hrsh7th/nvim-cmp',
  dependencies = {
    'hrsh7th/cmp-nvim-lsp', -- lsp as a completion source
    'hrsh7th/cmp-buffer',   -- buffer as a completion source
    'hrsh7th/cmp-path',     -- path as a completion source
  },
  event = 'InsertEnter',
  config = function ()
    -- Configure cmp with additional packages
    local luasnip = require 'luasnip'
    local cmp = require 'cmp'
    local cmp_autopairs = require 'nvim-autopairs.completion.cmp'
    cmp.setup {
      -- Sources are sorted by priority
      sources = {
        { name = "nvim_lsp" },
        { name = "buffer" },
        { name = "luasnip" },
        { name = 'path' },
        { name = 'rg' },
      },
      -- Mappings for the auto completion menu
      mapping = {
        ["<cr>"] = cmp.mapping.confirm({ select = true }),
        ["<s-tab>"] = cmp.mapping.select_prev_item(),
        ["<tab>"] = cmp.mapping.select_next_item(),
      },
      -- Add snippet support
      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body)
        end,
      },
      -- Format the entries in the auto completion menu
      formatting = {
        format = function(entry, item)
          item.kind = lsp_symbols[item.kind]
          item.menu = ({
            buffer = "[Buffer]",
            nvim_lsp = "[LSP]",
            luasnip = "[Snippet]",
          })[entry.source.name]

          return item
        end,
      },
      style = {
        winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
      },
      window = {
        completion = {
          border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
          scrollbar = "║",
          winhighlight = 'Normal:CmpMenu,FloatBorder:CmpMenuBorder,CursorLine:CmpSelection,Search:None',
          autocomplete = {
            require("cmp.types").cmp.TriggerEvent.InsertEnter,
            require("cmp.types").cmp.TriggerEvent.TextChanged,
          },
        },
        documentation = {
          border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
          winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
          scrollbar = "║",
        },
      },
      experimental = {
        native_menu = false,
        ghost_text = true,
      },
    }
    -- Attach autopairs
    cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))
  end
}
