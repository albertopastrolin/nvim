-- LSP servers to load and configure
return {
  'astro',
  'cssls',
  'dockerls',
  'eslint',
  'graphql',
  'html',
  'jsonls',
  'tsserver',
  'denols',
  'rust_analyzer',
  'sumneko_lua',
  'prosemd_lsp',
  'solc',
  'svelte',
  'tailwindcss',
  'volar',
  'yamlls'
}
